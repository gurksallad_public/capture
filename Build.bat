@ECHO OFF
REM Buildtool for outputing windows or wasm binarys
REM Version 0.0
REM Created By David isaksson 20221127
REM TODOS:
REM     Remove sdl2.lib from build folder
REM     Add different buildpath for release and develop
CLS
ECHO Buildtool V1.

SET Arg1=%1
SET Arg2=%2
SET Arg3=%3

REM Global options
SET FileToCompile=main.cpp
SET FileToOutput=Capture

IF NOT EXIST Build MKDIR Build


IF "%Arg1%"=="" (
    ECHO Compiling windows binarys with microsoft cl
    ECHO Building from %FileToCompile%

    REM Build options
    SET CopilerFlags=-MDd -nologo -fp:fast -Gm- -GR- -EHa- -Od -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -FC -Zi -DWINDOWSBUILD

    IF NOT EXIST Build\Windows MKDIR Build\Windows
    PUSHD Build\Windows

    cl %CopilerFlags% ..\..\Source\%FileToCompile% -Fe%FileToOutput%.exe -I ..\..\Source\Deps\sdl -I \Deps\sdl\backends SDL2.lib SDL2main.lib Opengl32.lib glew32.lib Gdi32.lib
    POPD
)

IF "%Arg1%"=="-wasm" (
    ECHO Compiling wasm binarys with emscripten
    ECHO Building from %FileToCompile%

    
    IF NOT EXIST Build\Wasm MKDIR Build\Wasm
    PUSHD Build\Wasm

    SET IMGUI_DIR=..\..\Source\Deps\imgui\
    SET SOURCES=%IMGUI_DIR%imgui.cpp %IMGUI_DIR%imgui_demo.cpp %IMGUI_DIR%imgui_draw.cpp %IMGUI_DIR%imgui_tables.cpp %IMGUI_DIR%imgui_widgets.cpp %IMGUI_DIR%backends/imgui_impl_sdl.cpp %IMGUI_DIR%backends/imgui_impl_opengl3.cpp

    emcc ..\..\Source\%FileToCompile% %SOURCES% -s WASM=1 -s USE_SDL=2 -s -O2 --preload-file ..\..\assets\sounds@ -s ALLOW_MEMORY_GROWTH=1 -o %FileToOutput%.js

    ECHO COPY %FileToOutput%.js TO PUBLIC DIR..\..\
    DEL ..\..\Public\%FileToOutput%.js 
    COPY %FileToOutput%.js ..\..\Public
    ECHO COPY %FileToOutput%.wasm TO PUBLIC DIR
    DEL ..\..\Public\%FileToOutput%.wasm 
    COPY %FileToOutput%.wasm ..\..\Public
    ECHO DONE!

    POPD
)