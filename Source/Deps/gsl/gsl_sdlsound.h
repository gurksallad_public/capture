//  Gurksallad Standard Library
//  Gamesound utility 1.0
//  Create sound to your game
//  By David isaksson 2022-12-10
//  Sound refers to the whole sound system
//  Audio refers to one audiofile laying or quing or audioslot 

// To play a sound... (This could be simplified)
//      gsl_game_filehandle fh = services->FileIO.LoadFile("storm.wav");                // Load file from disk
//      gsl_gameaudio_WaveFileData audio = gsl_gameaudio_LoadWaveAudioFile(fh.Data);    // retreive wavesound from loaded file
//      services->FileIO.FreeFile(fh);                                                  // Free file
//      gsl_game_audio_desc wavSoundDescriptor;                                         // Create an audio descriptor
//      wavSoundDescriptor.Data = audio.Data;                                           // Pointit to the extracted audio
//      wavSoundDescriptor.DataLenght = audio.Size;                                     // set audio size
//      wavSoundDescriptor.Note = 220.0f;                                               // hz modifier
//      wavSoundDescriptor.Volume = 1.0f;                                               // volumemodifier
//      wavSoundDescriptor.State = gsl_game_audiostates::New;                           // set its state to new
//      gsl_gameaudio_pushaudio(&handle->SoundDevice, &wavSoundDescriptor);             // play sound



//  Header
#ifndef GSL_SDLSOUND_H
#define GSL_SDLSOUND_H

#include "gsl_types.h"
#include <stdio.h>

#define GSL_GAME_MAXSOUNDBANKSLOTS     64      // Max number of audiofiles able to play at the same time
#define CreateRiffIdentifyer(c1, c2, c3, c4) ((uint8)c4 << 24 | (uint8)c3 << 16 | (uint8)c2 << 8 | (uint8)c1)

void gsl_gameaudio_callback(void *userdata, uint8 *stream, int len);

// REMOVE THOSE LINES ON COMPILE
#define GSL_SDLSOUND_IMPLEMENTATION
#include "../sdl/SDL.h"


//
struct gsl_game_sound_ring_buffer
{
    int Size;
    int WriteCursor;
    int PlayCursor;
    void *Data;
};

enum gsl_game_audiostates
{
    Free,
    New,
    InProgress,
    Pause,
    Abort,
    Done,
};

//
struct gsl_game_audio_desc
{
    gsl_game_audiostates State;
    int16* Data;
    uint64 DataLenght;
    float ReadPointer;
    float Note;             // Remap sound to selected note
    float Volume;
    float BalanceLeft;
    float BalanceRight;
};

//
struct gsl_game_sounddevice_desc
{
    gsl_game_audio_desc AudioBank[GSL_GAME_MAXSOUNDBANKSLOTS];
    gsl_game_sound_ring_buffer RingBuffer;
    float MasterVolume;
    bool DeviceReady;                                               // Sets to true after first sound update, checked in pushaudo to se if device is ready.
};

//
struct gsl_game_sound
{
    void* PushSound;                                //
    void* Stop;                                     //
    void* Pause;                                    //
    void* SetVolume;                                //
};

struct gsl_gameaudio_WaveFileData
{
	uint32 Size;
	int16* Data;
};

#pragma pack(push, 1)
struct WaveAudioFileHeader
{
	uint8 ChunkId[4];
	int32 ShunkSize;
	uint8 Format[4];
};
struct WaveAudioFileFmtChunk
{
	uint8 ChunkId[4];
	uint32 ChunkSize;
	uint16 FormatTag;
	uint16 Channels;
	uint32 SamplesPerSeconds;
	uint32 AvgBytesPerSecond;
	uint16 BlockAlign;
	uint16 BitsPerSample;
	uint16 Size;
	uint16 ValidBitsPerSample;
	uint32 ChannelMask;
	uint8 SubFormat[16];
};
struct waveAudioFilePack
{
	WaveAudioFileHeader header;
	WaveAudioFileFmtChunk Chunk;
};
struct RiffChunkInfo
{
	union
	{
		char ctype[4];
		uint32 iType;
	};
	
	uint32 size;
};
#pragma pack(pop)
#endif



//  Implementation
#ifdef GSL_SDLSOUND_IMPLEMENTATION

int gsl_GameSound_Initialize(gsl_game_sounddevice_desc* device)
{
    int SamplesPerSecond = 48000;

    SDL_AudioSpec audioSettings = {};

    audioSettings.freq = SamplesPerSecond;
    audioSettings.format = AUDIO_S16LSB;
    audioSettings.channels = 2;
    audioSettings.samples = 512;
    audioSettings.callback = &gsl_gameaudio_callback;
    audioSettings.userdata = device;

   
	if (SDL_OpenAudio(&audioSettings, NULL) < 0 ){
	  fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
	  return(-1);
	}

    if (audioSettings.format != AUDIO_S16LSB)
    {
	  printf("Could not retreive a soundbuffer fro little endian system... \n");
	  return(-1);
    }

    device->RingBuffer.Size = SamplesPerSecond;
    device->RingBuffer.Data = calloc(1, SamplesPerSecond);
    device->RingBuffer.PlayCursor = 0;
    device->RingBuffer.WriteCursor = 0;
    device->MasterVolume = 0.5f;
    SDL_PauseAudio(0);
    return 1;
}



// Update calback from SDL to update soundbuffer. 
void gsl_gameaudio_callback(void *UserData, Uint8 *AudioData, int Length)
{
    gsl_game_sounddevice_desc* device = (gsl_game_sounddevice_desc*)UserData;
    device->DeviceReady = true;

    for(int i = 0; i < Length/2; i+=2)
    {
        float sampleLeft = 0;
        float sampleRight = 0;

        for(uint32 j = 0; j < GSL_GAME_MAXSOUNDBANKSLOTS; ++j)
        {
            if(&device->AudioBank[j] != 0)
            {
                if(device->AudioBank[j].State == gsl_game_audiostates::InProgress)
                {
                    float readOffset = device->AudioBank[j].ReadPointer * 2.0f;
                    uint32 sample1Right = (uint32)readOffset - ((uint64)readOffset % 2);
                    uint32 sample1Left = (uint32)readOffset - ((uint64)readOffset % 2) + 1;

                    if(sample1Left * 2 < device->AudioBank[j].DataLenght)
                    {
                        float left = (float)device->AudioBank[j].Data[sample1Left];
                        float right = (float)device->AudioBank[j].Data[sample1Right];
                        sampleLeft += left * device->AudioBank[j].Volume * device->AudioBank[j].BalanceLeft;
                        sampleRight += right * device->AudioBank[j].Volume * device->AudioBank[j].BalanceRight;

                        device->AudioBank[j].ReadPointer += 0.5f * device->AudioBank[j].Note;
                    }
                    else
                    {
                        device->AudioBank[j].State = gsl_game_audiostates::Done;
                    }
                }
            }
        }
        int16* audioStream = (int16*)(AudioData);
        audioStream[i] = (int16)(sampleLeft * device->MasterVolume);
        audioStream[i+1] = (int16)(sampleRight * device->MasterVolume);
        // device->AudioBank.SoundBuffers.PlotBuffer[i] = (float)sampleLeft / 32767.0f;
        // device->AudioBank.SoundBuffers.PlotBuffer[i + 1] = (float)sampleRight / 32767.0f;
    }
}



// adds a audio to the playque, returns false if audio could not be queed
static bool gsl_gameaudio_pushaudio(gsl_game_sounddevice_desc* device, gsl_game_audio_desc* audio, float note = 220.0f)
{
    if(device->DeviceReady == false) return false;
    bool result = false;

    for(int i = 0; i < GSL_GAME_MAXSOUNDBANKSLOTS; ++i)
    {
        if(device->AudioBank[i].State == gsl_game_audiostates::Free)
        {
            device->AudioBank[i].Data = audio->Data;
			device->AudioBank[i].DataLenght = audio->DataLenght;
			device->AudioBank[i].ReadPointer = 0;
			device->AudioBank[i].State = gsl_game_audiostates::InProgress;
			device->AudioBank[i].Note = note / 220.0f;
			device->AudioBank[i].Volume = audio->Volume;
			device->AudioBank[i].BalanceLeft = 1.0f;
			device->AudioBank[i].BalanceRight = 1.0f;
            result = true;
            break;
        }
    }

    return result;
}

static gsl_gameaudio_WaveFileData gsl_gameaudio_LoadWaveAudioFile(void* audioData)
{
	uint32 riffinfo_Pcm = CreateRiffIdentifyer('P', 'C', 'M', 32);
	uint32 riffinfo_Wave = CreateRiffIdentifyer('W', 'A', 'V', 'E');
	uint32 riffinfo_Riff = CreateRiffIdentifyer('R', 'I', 'F', 'F');
	uint32 riffinfo_Data = CreateRiffIdentifyer('d', 'a', 't', 'a');
	uint32 riffinfo_Fact = CreateRiffIdentifyer('f', 'a', 'c', 't');
	uint32 riffinfo_Cue = CreateRiffIdentifyer('c', 'u', 'e', 32);

	//waveAudioFile->Data;
	waveAudioFilePack* header = (waveAudioFilePack*)audioData;
	uint8* read = (uint8*)&header->Chunk.FormatTag + header->Chunk.ChunkSize;
	RiffChunkInfo* rci = (RiffChunkInfo*)read;
	uint32 v = *(uint32*)header->header.ChunkId;

	Assert(*(uint32*)header->header.ChunkId == riffinfo_Riff);
	Assert(*(uint32*)header->header.Format == riffinfo_Wave);
	Assert(rci->iType != riffinfo_Fact);												// Compressed wavefiles is not supported
	Assert(header->Chunk.SamplesPerSeconds == SOUND_OUTPUTSAMPLERATE)					// Wrong SampleRate on inputfile, Must be 48000 bits per seconds.
	gsl_gameaudio_WaveFileData result = {};

	if (rci->iType == riffinfo_Cue)
	{
		read += rci->size + 8;
		rci = (RiffChunkInfo*)read;
	}
	if(rci->iType == riffinfo_Data)
	{
		result.Size = rci->size;
        printf("size %u\n", result.Size);
		result.Data = (int16*)malloc(result.Size);
		int16* write = result.Data;
		int16* xread = (int16*)rci + 8;
		for (uint32 i = 0; i < result.Size / 2; i++)
		{ 
			*write++ = *xread++;
		}
	}

	return result;
}
#endif