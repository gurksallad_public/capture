/*
Author: David Isaksson
Created:
Description:

    Ludo math
        Version 2114A
        210410
            Added Matrix Rotation For X and Z Axis
            Corecting bugg in LookAt Matrix creation
        221210
            Renamed to gsl_math.h
            Added clampfunctions
*/

#ifndef GSL_MATH_H
#define GSL_MATH_H

#include "gsl_types.h"
#include <math.h>
//#include <xmmintrin.h>

#define Pi32 3.14159265359f
#define Pi32Over2 1.57079632679f
#define Pi32Over4 0.78539816339f
#define TAU32 6.28318530717958647692f
#define RAD32 0.0174533f



// Truncate
inline uint32 SafeTruncateUInt64(uint64 value)
{
    Assert(value<= 0xFFFFFFFF);
    uint32 result = (uint32)value;
    return (uint32)value;
}
inline uint16 SafeTruncateUInt64(uint32 value)
{
    Assert(value<= 0xFFFF);
    uint16 result = (uint16)value;
    return (uint16)value;
}
inline uint8 SafeTruncateUInt64(uint16 value)
{
    Assert(value<= 0xFF);
    uint8 result = (uint8)value;
    return (uint8)value;
}

inline float RotatedColor(float time, float offset)
{
    float result = sinf(time + offset);

    return result;
}


// Clamps
inline float Clamp(float value, float min, float max)
{
    float result = value;
    if (result > max) result = max;
    if (result < min) result = min;

    return result;
}


static inline float
SquareRoot(float val)
{
    //float result = _mm_cvtss_f32(_mm_sqrt_ss(_mm_set_ss(val)));
    float result = (float)sqrt(val);
    return result;
}

static inline float
Lerp(float a, float b, float t)
{
    float result = a + t * (b - a);
    return result;
}

static inline float
Sin(float rad)
{
    float result = sinf(rad);
    return result;
}

static inline float
Cos(float rad)
{
    float result = cosf(rad);
    return result;
}

static inline void
SwapSort(int* a, int* b)
{
    if(*a > *b)
    {
        int t = *a;
        *a = *b;
        *b = t;
    }
}


static v2 LineSegmentIntersection(v2 p0, v2 p1, v2 p2, v2 p3)
{
    float A1 = p1.Y - p0.Y;
    float B1 = p0.X - p1.X;
    float C1 = A1 * p0.X + B1 * p0.Y;
    float A2 = p3.Y - p2.Y;
    float B2 = p2.X - p3.X;
    float C2 = A2 * p2.X + B2 * p2.Y;
    float denominator = A1 * B2 - A2 * B1;

    if (denominator == 0)
    {
        return v2{0, 0};
    }

    float intersectX = (B2 * C1 - B1 * C2) / denominator;
    float intersectY = (A1 * C2 - A2 * C1) / denominator;
    float rx0 = (intersectX - p0.X) / (p1.X - p0.X);
    float ry0 = (intersectY - p0.Y) / (p1.Y - p0.Y);
    float rx1 = (intersectX - p2.X) / (p3.X - p2.X);
    float ry1 = (intersectY - p2.Y) / (p3.Y - p2.Y);

    if (((rx0 >= 0 && rx0 <= 1) || (ry0 >= 0 && ry0 <= 1)) &&
        ((rx1 >= 0 && rx1 <= 1) || (ry1 >= 0 && ry1 <= 1)))
    {
        return v2{intersectX, intersectY};
    }
    else
    {
        return v2{0, 0};
    }
}

// this should be moved to a string header
static inline float
_InternalParseFloat(char* string)
{
    float result = 0;
    float natural = 0;
    float fraction = 0;
    char* read = string;
    float isNegative = 1;
    while(*read == '.' || *read == '-' ||  (*read >= 48 && *read <= 57))
    {
	    switch(*read)
        {
            case '.':
            {
	            if(fraction > 0) *read = 0;
                fraction = 1;
            } break;
            case '-':
            {
                if(isNegative == -1) *read = 0;
	            isNegative = -1;
            } break;
            default:
            {
                if(fraction > 0) fraction *= 10;
                natural = natural * 10;
                natural += (*read - 48);
            }
	    }
        if (*read == 0) break;
        read++;
    }
    result = isNegative * (natural / fraction);
    return result;
}


static inline float
Random_Lehmer(uint32 *state)
{
	uint64 product = (uint64)*state * 48271;
	uint32 x = (uint32)((product & 0x7fffffff) + (product >> 31));

	x = (x & 0x7fffffff) + (x >> 31);
	return (float)(*state = x) / (float)0x7FFFFFFF;
}


/*#################################################################################*/
/* --------------                      Vector2                       --------------*/
/*#################################################################################*/

static inline bool
operator== (v2 v2a, v2 v2b)
{
    bool result1 = (int)v2a.X == (int)v2b.X;
    bool result2 = (int)v2a.Y == (int)v2b.Y;
    return result1 && result2;
}

static inline bool
operator!= (v2 v2a, v2 v2b)
{
    bool result;
    result = !(v2a == v2b);
    return result;
}

static inline v2
operator- (v2 v2a, v2 v2b)
{
    v2 result;
    result.X = v2a.X - v2b.X;
    result.Y = v2a.Y - v2b.Y;
    return result;
}

static inline v2
operator+ (v2 v2a, v2 v2b)
{
    v2 result;
    result.X = v2a.X + v2b.X;
    result.Y = v2a.Y + v2b.Y;
    return result;
}

static inline v2
operator/ (v2 v2a, float val)
{
    v2 result;
    result.X = v2a.X / val;
    result.Y = v2a.Y / val;
    return result;
}

static inline v2
operator* (float val, v2 vector2)
{
    v2 result;
    result.X = val * vector2.X;
    result.Y = val * vector2.Y;
    return result;
}

static inline v2
operator* (v2 vector2, float val)
{
    v2 result = val * vector2;
    return result;
}

static inline v2 _InternalVector2Zero(void) { return {0, 0}; }
static inline v2 _InternalVector2One(void) { return {1, 1}; }
static inline v2 V2(float x, float y)
{
    v2 result;

    result.X = x;
    result.Y = y;

    return result;
}
static inline float _InternalVector2Inner(v2 v2a, v2 v2b)
{
    float result = v2a.X * v2b.X + v2a.Y * v2b.Y;
    return result;
}
static inline float _InternalVector2LengthSq(v2 vector2)
{
    float result = _InternalVector2Inner(vector2, vector2);
    return result;
}
static inline float _InternalVector2Length(v2 vector2)
{
    float result = SquareRoot(_InternalVector2LengthSq(vector2));
    return result;
}
static inline v2 _InternalVector2Normalize(v2 vector2)
{
    float lenght = _InternalVector2Length(vector2);
    v2 result = vector2;
    if (lenght != 0) result = vector2 * (1.0f / lenght);
    return result;
}
static inline v2 _InternalVector2Reflect(v2 vector, v2 normal)
{
    v2 result;
    float val = 2.0f * ((vector.X * normal.X) + (vector.Y * normal.Y));
    result.X = vector.X - (normal.X * val);
    result.Y = vector.Y - (normal.Y * val);
    return result;
}
static inline float _InternalVector2ToRadians(v2 vec)
{
    float result;
    result = atan2f(vec.Y, -vec.X);
    return result;
}
static inline v2 _InternalVector2FromAngle(float rad)
{
    v2 result;
    result.X = -Cos(rad);
    result.Y = Sin(rad);
    return result;
}

static Vector2Types Vector2;
static void Vector2Init()
{
    Vector2.New = &V2;
    Vector2.Zero = &_InternalVector2Zero;
    Vector2.One = &_InternalVector2One;
    Vector2.Normalize = &_InternalVector2Normalize;
    Vector2.Reflect = &_InternalVector2Reflect;
    Vector2.Length = &_InternalVector2Length;
    Vector2.LengthSqr = &_InternalVector2LengthSq;
    Vector2.ToRadians = &_InternalVector2ToRadians;
    Vector2.FromAngle = &_InternalVector2FromAngle;
}


/*#################################################################################*/
/* --------------                      Vector3                       --------------*/
/*#################################################################################*/

// Operator overloading
static inline v3
operator- (v3 v3a, v3 v3b)
{
    v3 result;
    result.X = v3a.X - v3b.X;
    result.Y = v3a.Y - v3b.Y;
    result.Z = v3a.Z - v3b.Z;
    return result;
}

static inline v3
operator/ (v3 v3a, float val)
{
    v3 result;
    result.X = v3a.X / val;
    result.Y = v3a.Y / val;
    result.Z = v3a.Z / val;
    return result;
}

static inline v3
operator* (float val, v3 vector3)
{
    v3 result;
    result.X = val * vector3.X;
    result.Y = val * vector3.Y;
    result.Z = val * vector3.Z;
    return result;
}

static inline v3
operator+ (v3 vector3, v3  rhs)
{
    v3 result;
    result.X = vector3.X + rhs.X;
    result.Y = vector3.Y + rhs.Y;
    result.Z = vector3.Z + rhs.Z;
    return result;
}

static inline v3
operator+ (v3 vector3, float val)
{
    v3 result;
    result.X = vector3.X + val;
    result.Y = vector3.Y + val;
    result.Z = vector3.Z + val;
    return result;
}

static inline v3
operator* (v3 vector3, float val)
{
    v3 result = val * vector3;
    return result;
}
// Constructor
static inline v3 V3(float x, float y, float z)
{
    v3 result;

    result.X = x;
    result.Y = y;
    result.Z = z;

    return result;
}
static inline v3 V3(v2 v, float z)
{
    v3 result;

    result.X = v.X;
    result.Y = v.Y;
    result.Z = z;

    return result;
}
static inline v3 V3(p3 p)
{
    v3 result;

    result.X = (float)p.X;
    result.Y = (float)p.Y;
    result.Z = (float)p.Z;

    return result;
}


// Internal functions
static inline v3 _InternalVector3Zero(void) { return {0, 0, 0}; }
static inline v3 _InternalVector3One(void) { return {1, 1, 1}; }
static inline v3 _InternalVector3Up(void) { return {0, 1, 0}; }
static inline v3 _InternalVector3Down(void) { return {0, -1, 0}; }
static inline v3 _InternalVector3Left(void) { return {-1, 0, 0}; }
static inline v3 _InternalVector3Right(void) { return {1, 0, 0}; }
static inline v3 _InternalVector3Front(void) { return {0, 0, -1}; }
static inline v3 _InternalVector3Back(void) { return {0, 0, 1}; }
static inline float _InternalVector3Inner(v3 v3a, v3 v3b)
{
    float result = v3a.X * v3b.X + v3a.Y * v3b.Y + v3a.Z * v3b.Z;
    return result;
}
static inline float
_InternalVector3LengthSq(v3 vector3)
{
    float result = _InternalVector3Inner(vector3, vector3);
    return result;
}
static inline float _InternalVector3Length(v3 vector3)
{
    float result = SquareRoot(_InternalVector3LengthSq(vector3));
    return result;
}
static inline v3 _InternalVector3Normalize(v3 vector3)
{
    float lenght = _InternalVector3Length(vector3);
    v3 result = vector3 * (1.0f / lenght);
    return result;
}
static inline v3 _InternalVector3Cross(v3 v3a, v3 v3b)
{
    v3 result;
    //result.X = v3a.Y * v3b.Z - v3a.Z * v3b.Y;
    //result.Y = v3a.Z * v3b.X - v3a.X * v3b.Z;
    //result.Z = v3a.X * v3b.Y - v3a.Y * v3b.X;

    result.X = v3a.Y * v3b.Z - v3b.Y * v3a.Z;
    result.Y = -(v3a.X * v3b.Z - v3b.X * v3a.Z);
    result.Z = v3a.X * v3b.Y - v3b.X * v3a.Y;
    return result;
}
static inline v3 _InternalVector3Transform(v3 v3in, matrix m)
{
    v3 result ={};
    result.X = (v3in.X * m.E[0][0]) + (v3in.Y * m.E[1][0]) + (v3in.Z * m.E[2][0]) + m.E[3][0];
    result.Y = (v3in.X * m.E[0][1]) + (v3in.Y * m.E[1][1]) + (v3in.Z * m.E[2][1]) + m.E[3][1];
    result.Z = (v3in.X * m.E[0][2]) + (v3in.Y * m.E[1][2]) + (v3in.Z * m.E[2][2]) + m.E[3][2];

    return result;
}

// Function lib
static Vector3Types Vector3;
static void Vector3Init()
{
    Vector3.New = &V3;
    Vector3.Zero = &_InternalVector3Zero;
    Vector3.One = &_InternalVector3One;
    Vector3.Up = &_InternalVector3Up;
    Vector3.Down = &_InternalVector3Down;
    Vector3.Left = &_InternalVector3Left;
    Vector3.Right = &_InternalVector3Right;
    Vector3.Front = &_InternalVector3Front;
    Vector3.Back = &_InternalVector3Back;
    Vector3.Normalize = &_InternalVector3Normalize;
    Vector3.Length = &_InternalVector3Length;
    Vector3.LengthSqr = &_InternalVector3LengthSq;
    Vector3.InnerProduct = &_InternalVector3Inner;
    Vector3.CrossProduct = &_InternalVector3Cross;
    Vector3.Transform = &_InternalVector3Transform;
}


/* -------------- Vector4  --------------*/
/* -------------- Matrices --------------*/

// OPerator overloading
static inline matrix
operator* (matrix left, matrix right)
{
    // TODO: Optimize
    matrix result = {};
    for(int r = 0; r <= 3; ++r) // Rows of A
    {
        for(int c = 0; c <= 3; ++c) // Column of B
        {
            for(int i = 0; i <= 3; ++i) // Columns of A, rows of B
            {
                result.E[r][c] += left.E[r][i] * right.E[i][c];
            }
        }
    }

    return result;
}

// Internal functions
static inline matrix
_InternalIdentityMatrix()
{
    matrix result =
    {{
        { 1, 0, 0, 0 },
        { 0, 1, 0, 0 },
        { 0, 0, 1, 0 },
        { 0, 0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalFlipMatrix()
{
    matrix result =
    {{
        { 1.0f, 0.0f, 0.0f, 0.0f },
        { 0.0f, -1.0f, 0.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 0.0f, 1.0f },
    }};

    return result;
}

static inline matrix
_InternalMirrorMatrix()
{
    matrix result =
    {{
        { -1.0f, 0.0f, 0.0f, 0.0f },
        { 0.0f, -1.0f, 0.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 0.0f, 1.0f },
    }};

    return result;
}

static inline matrix
_InternalScaleMatrix(float scalar)
{
    matrix result =
    {{
        { scalar, 0, 0, 0 },
        { 0, scalar, 0, 0 },
        { 0, 0, scalar, 0 },
        { 0, 0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalScale2DMatrix(float width, float height)
{
    matrix result =
    {{
        { width, 0, 0, 0 },
        { 0, height, 0, 0 },
        { 0, 0, 10, 0 },
        { 0, 0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalMatrixRotateXMatrix(float angle)
{
    float c = cosf(angle);
    float s = sinf(angle);

    matrix result =
    {{
        {  1,  0, 0, 0 },
        {  0,  c, s, 0 },
        {  0, -s, c, 0 },
        {  0,  0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalMatrixRotateYMatrix(float angle)
{
    float c = cosf(angle);
    float s = sinf(angle);

    matrix result =
    {{
        {  c, 0, s, 0 },
        {  0, 1, 0, 0 },
        { -s, 0, c, 0 },
        {  0, 0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalMatrixRotateZMatrix(float angle)
{
    float c = cosf(angle);
    float s = sinf(angle);

    matrix result =
    {{
        {  s, c, 0, 0 },
        { -c, s, 0, 0 },
        {  0, 0, 1, 0 },
        {  0, 0, 0, 1 }
    }};

    return result;
}

static inline matrix
_InternalMatrixTranslateMatrix(v3 offset)
{
    matrix result =
    {{
        { 1, 0, 0, 0 },
        { 0, 1, 0, 0 },
        { 0, 0, 1, 0 },
        { offset.X, offset.Y, offset.Z, 1 }
    }};

    return result;
}

static inline matrix
_InternalMatrixPerspectiveProjectionMatrix(float fov, float aspectRatio, float nearClipp, float farClipp)
{
    float yScale = 1.0f / tanf(fov * 0.5f);
    float xScale = yScale / aspectRatio;
    float range = farClipp / (nearClipp - farClipp);
    float negOne = -1;

    matrix result =
    {{
        { xScale,       0.0f,       0.0f,               0.0f },
        { 0.0f,         yScale,     0.0f,               0.0f },
        { 0.0f,         0.0f,       range,              negOne },
        { 0.0f,         0.0f,       nearClipp * range,  0.0f },
    }};

    return result;
}

static inline matrix
_InternalMatrixOrthographiclProjectionMatrix(float width, float height, float nearClipp, float farClipp)
{
    float a = 2.0f / width;
    float b = 2.0f / height;
    float range = 1.0f / (nearClipp - farClipp);
    float offRange = nearClipp / (nearClipp - farClipp);

    matrix result =
    {{
        { a,            0.0f,       0.0f,               0.0f },
        { 0.0f,         b,          0.0f,               0.0f },
        { 0.0f,         0.0f,       range,              0.0f },
        { 0.0f,         0.0f,       offRange,           1.0f },
    }};

    return result;
}

static inline matrix
_InternalMatrixOffCenterOrthographiclProjectionMatrix(float left, float right, float bottom, float top, float nearClipp, float farClipp)
{
    float a = 2.0f / (right - left);
    float b = 2.0f / (top - bottom);
    float range = 1.0f / (nearClipp - farClipp);
    float offRange = nearClipp / (nearClipp - farClipp);
    float width = (float)((left + right) / (left - right));
    float height = (float)((top + bottom) / (bottom - top));
    matrix result =
    {{
        { a,            0.0f,       0.0f,               0.0f },
        { 0.0f,         b,          0.0f,               0.0f },
        { 0.0f,         0.0f,       range,              0.0f },
        { width,        height,     offRange,           1.0f },
    }};

    return result;
}

static inline matrix
_InternalMatrixLookAtMatrix(v3 target, v3 position, v3 up)
{
    v3 zaxis = _InternalVector3Normalize(target - position);
    v3 xaxis = _InternalVector3Normalize(_InternalVector3Cross(up, zaxis));
    v3 yaxis = _InternalVector3Cross(zaxis, xaxis);

    matrix orientation =
    {{
       { xaxis.X, yaxis.X, zaxis.X, 0 },
       { xaxis.Y, yaxis.Y, zaxis.Y, 0 },
       { xaxis.Z, yaxis.Z, zaxis.Z, 0 },
       { 0,       0,       0,     1 }
    }};

    matrix translation =
    {{
        { 1,            0,              0,              0 },
        { 0,            1,              0,              0 },
        { 0,            0,              1,              0 },
        { -target.X,    -target.Y,      -target.Z,      1 }
    }};

    return (translation * orientation);
}

static v2 _InternalMatrixTransformVector2(v2 position, matrix m)
{
    v2 result;
    result = V2((position.X * m.E[0][0]) + (position.Y * m.E[1][0]) + m.E[3][0], (position.X * m.E[0][1]) + (position.Y * m.E[1][1]) + m.E[3][1]);

    return result;
}


static MatrixTypes Matrix;
static void MatrixInit()
{
    Matrix.Identity = &_InternalIdentityMatrix;
    Matrix.Scale = &_InternalScaleMatrix;
    Matrix.RotateX = &_InternalMatrixRotateXMatrix;
    Matrix.RotateY = &_InternalMatrixRotateYMatrix;
    Matrix.RotateZ = &_InternalMatrixRotateZMatrix;
    Matrix.PerspectiveProjection = &_InternalMatrixPerspectiveProjectionMatrix;
    Matrix.OrthographicProjection = &_InternalMatrixOrthographiclProjectionMatrix;
    Matrix.OrthographicProjectionOffCenter = &_InternalMatrixOffCenterOrthographiclProjectionMatrix;
    Matrix.LookAt = &_InternalMatrixLookAtMatrix;
    Matrix.Mirror = &_InternalMirrorMatrix;
    Matrix.Flip = &_InternalFlipMatrix;
    Matrix.Translate = &_InternalMatrixTranslateMatrix;
    Matrix.TransformVector2 = &_InternalMatrixTransformVector2;
    Matrix.Scale2D = &_InternalScale2DMatrix;
}

#endif