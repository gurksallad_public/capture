#ifndef GSL_SDL_WASMLAYER_H
#define GSL_SDL_WASMLAYER_H

#include "gsl_types.h"
#include "../imgui/imgui.h"
#include "../imgui/backends/imgui_impl_sdl.h"
// #include "../imgui/imgui.cpp"
// #include "../imgui/imgui_widgets.cpp"
// #include "../imgui/imgui_draw.cpp"
// #include "../imgui/imgui_tables.cpp"
// #include "../imgui/imgui_demo.cpp"

//#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#include "../imgui/backends/imgui_impl_opengl3.h"
// #include "../imgui/backends/imgui_impl_opengl3.cpp"
// #include <SDL.h>
// #include <SDL_opengles2.h>
//#include "../imgui/backends/imgui_impl_sdl.cpp"

#include <stdio.h>
#include <emscripten.h>
#include <SDL.h>
#include <SDL_opengles2.h>

#endif

//  Implementation
#ifdef GSL_SDLGAME_IMPLEMENTATION

    // Maybe do something better with this?
    static gsl_game_handle* _gsl_internal_gameHandle = {};
    static gsl_game_services* _gsl_internal_gameServices = {};

    // static test shader to get going
    static char* gsl_DefaultVertexShader = (char*)
    "\n"
    "\n" 
    "   attribute vec2 VertexPos2D;\n"
    "\n"
    "   void main()\n"
    "   {\n"
    "       gl_Position = vec4(VertexPos2D.x, VertexPos2D.y, 0, 1);\n"
    "   }\n"
    "\n"
    "\n";

    static char* gsl_DefaultFragmentShader = (char*)
    "\n"
    "\n"
    "   void main()\n"
    "   {\n"
    "       gl_FragColor = vec4(1.0, 0.71, 0.2, 1.0);\n" // 
    "   }\n"
    "\n"
    "\n";



    //
    static void gsl_internal_wasmFreeFile(gsl_game_filehandle fileHandle)
    {
        if(fileHandle.Data)
        {
            free(fileHandle.Data);
        }
        fileHandle.ContentSize = 0;
        fileHandle.Data = 0;
    }



    // Not implemeted
    static bool gsl_internal_wasmSaveFile(char* filePathName, uint32 contentSize, void* data)
    {
        bool result = false;
        printf("Error: gsl_internal_wasmSaveFile has no implementation\n");
        return result;
    }



    //
    gsl_game_filehandle gsl_internal_wasmLoadFile(char* filePathName)
    {
        gsl_game_filehandle result = {};
        // rewrite this to a more safe and clean modle
        FILE *fileptr;
        char *buffer;
        long filelen;

        fileptr = fopen(filePathName, "rb");  // Open the file in binary mode
        fseek(fileptr, 0, SEEK_END);          // Jump to the end of the file
        filelen = ftell(fileptr);             // Get the current byte offset in the file
        rewind(fileptr);                      // Jump back to the beginning of the file

        buffer = (char *)malloc(filelen * sizeof(char)); // Enough memory for the file
        fread(buffer, filelen, 1, fileptr); // Read in the entire file
        fclose(fileptr); // Close the file

        result.ContentSize = (uint32)filelen;
        result.Data = buffer;
        //printf("Error: gsl_internal_windowsLoadFile has no implementation\n");
        return result;
    }

    

    void gsl_internal_wasmMainLoop()
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if(_gsl_internal_gameHandle->GameDesc->UseImGui) ImGui_ImplSDL2_ProcessEvent(&event);
            // Capture events here, based on io.WantCaptureMouse and io.WantCaptureKeyboard
        }

        if(_gsl_internal_gameHandle == 0)
        {
            printf("Error, int _gsl_internal_wasmManinLoop\n");
        }
        else
        {
            gsl_internal_TickLoop(_gsl_internal_gameHandle, _gsl_internal_gameServices);
        }
    }



    static bool gsl_internalwasm_game_initializeOpenGL(gsl_game_handle* handle)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        //glEnable(GL_BLEND);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        SDL_DisplayMode current;
        SDL_GetCurrentDisplayMode(0, &current);

        handle->GLContext = SDL_GL_CreateContext(handle->sdlWindow);
        if(handle->GLContext == 0)
        {
            // TODO logging
            printf("GL context creation faild\n");
            return false;
        }
        SDL_GL_SetSwapInterval(1); // Enable vsync
        handle->DefaultShader.DemoVertexShader = gsl_DefaultVertexShader;
        if(handle->GameDesc->InitializeFn == 0)
        {
            // If no init function is provided, provide a demo function instead...
            printf("Demo initializer...\n");
            handle->GameDesc->InitializeFn = &gsl_GameInitializeStubFn;
        }
        if(handle->GameDesc->RenderFn == 0)
        {
            printf("Demo renderloop set...\n");
            handle->GameDesc->RenderFn = &gsl_GameRenderStubFn;
        }
        return true;
    }

    static void GetPlatformServiceFunctions(gsl_game_services* services)
    {
        services->FileIO.LoadFile = &gsl_internal_wasmLoadFile;
        services->FileIO.SaveFile = &gsl_internal_wasmSaveFile;
        services->FileIO.FreeFile = &gsl_internal_wasmFreeFile;
    }

#endif