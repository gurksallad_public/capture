/*
Author: David Isaksson
Created:
Description:
*/

#ifndef DEFAULTTYPES_H
#define DEFAULTTYPES_H


/*
    Ludo Default types
        Version 2114A
*/

#include <stdint.h>
#include <math.h>



/* -------------- MACRO --------------*/
#if DEBUG_BUILD
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

#define ARRAYCOUNT(array) (sizeof(array)/sizeof(*(array)))


typedef int64_t		int64;
typedef uint64_t 	uint64;
typedef int32_t 	int32;
typedef uint32_t 	uint32;
typedef int16_t 	int16;
typedef uint16_t 	uint16;
typedef int8_t		int8;
typedef uint8_t		uint8;

typedef int32_t     bool32;

/* -------------- Matrix  --------------*/
struct matrix
{
    float E[4][4];
};

/* -------------- Vector2  --------------*/
union v2
{
    struct
    {
        float X;
        float Y;
    };
    struct
    {
        float U;
        float V;
    };
    struct
    {
        float Width;
        float Height;
    };
    float E[2];

    v2& operator+= (const v2& rhs)
    {
        this->X += rhs.X;
        this->Y += rhs.Y;
        return *this;
    }

    v2& operator-= (const v2& rhs)
    {
        this->X -= rhs.X;
        this->Y -= rhs.Y;
        return *this;
    }
};

typedef v2 internal_Vector2_type(void);
typedef v2 internal_Vector2_new(float, float);
typedef v2 internal_Vector2_type2a(v2);
typedef v2 internal_Vector2_type2b(v2, v2);
typedef float internal_Vector2_type2c(v2);
typedef v2 internal_Vector2_typeFromAngle(float);

struct Vector2Types
{
    internal_Vector2_new* New;
    internal_Vector2_type* Zero;
    internal_Vector2_type* One;
    internal_Vector2_type2a* Normalize;
    internal_Vector2_type2b* Reflect;
    internal_Vector2_type2c* LengthSqr;
    internal_Vector2_type2c* Length;
    internal_Vector2_type2c* ToRadians;
    internal_Vector2_typeFromAngle* FromAngle;
};



/* -------------- Point3  --------------*/
union p3
{
    struct
    {
        int32 X;
        int32 Y;
        int32 Z;
    };
    int32 E[3];

    p3& operator+= (const p3& rhs)
    {
        this->X += rhs.X;
        this->Y += rhs.Y;
        this->Z += rhs.Z;
        return *this;
    }

    p3& operator-= (const p3& rhs)
    {
        this->X -= rhs.X;
        this->Y -= rhs.Y;
        this->Z -= rhs.Z;
        return *this;
    }
};

static inline bool
operator== (p3 lhs, p3 rhs)
{
    bool result;
    result = lhs.X == rhs.X && lhs.Y == rhs.Y && lhs.Z == rhs.Z;
    return result;
}

/* -------------- Vector3  --------------*/
union v3
{
    struct
    {
        float X;
        float Y;
        float Z;
    };
    struct
    {
        float R;
        float G;
        float B;
    };
    struct
    {
        v2 XY;
        float _Z;
    };
    struct
    {
        float _X;
        v2 YZ;
    };
    float E[3];

    v3& operator+= (const v3& rhs)
    {
        this->X += rhs.X;
        this->Y += rhs.Y;
        this->Z += rhs.Z;
        return *this;
    }

    v3& operator-= (const v3& rhs)
    {
        this->X -= rhs.X;
        this->Y -= rhs.Y;
        this->Z -= rhs.Z;
        return *this;
    }
};

typedef v3 internal_Vector3_type(void);
typedef v3 internal_Vector3_new(float, float, float);
typedef v3 internal_Vector3_type3a(v3);
typedef float internal_Vector3_type3b(v3);
typedef v3 internal_Vector3_type33a(v3, v3);
typedef float internal_Vector3_type33b(v3, v3);
typedef v3 internal_Vector3_transform(v3, matrix);

struct Vector3Types
{
    internal_Vector3_new* New;
    internal_Vector3_type* Zero;
    internal_Vector3_type* One;
    internal_Vector3_type* Up;
    internal_Vector3_type* Down;
    internal_Vector3_type* Left;
    internal_Vector3_type* Right;
    internal_Vector3_type* Front;
    internal_Vector3_type* Back;
    internal_Vector3_type3a* Normalize;
    internal_Vector3_type3b* Length;
    internal_Vector3_type3b* LengthSqr;
    internal_Vector3_type33b* InnerProduct;
    internal_Vector3_type33a* CrossProduct;
    internal_Vector3_transform* Transform;
};



/* -------------- Vector4  --------------*/
union v4
{
    struct
    {
        float X;
        float Y;
        float Z;
        float W;
    };
    struct
    {
        float R;
        float G;
        float B;
        float A;
    };
    struct
    {
        v3 XYZ;
        float _W;
    };
    float E[4];
};

typedef v4 internal_Vector4_type(void);
typedef v4 internal_Vector4_new(float, float, float, float);

static inline v4
V4(float x, float y, float z, float w)
{
    v4 result;

    result.X = x;
    result.Y = y;
    result.Z = z;
    result.W = w;

    return result;
}

static inline v4 _InternalVector4Zero(void) { return {0, 0, 0, 0}; }
static inline v4 _InternalVector4One(void) { return {1, 1, 1, 1}; }

struct Vector4Types
{
    internal_Vector4_new* New;
    internal_Vector4_type* Zero;
    internal_Vector4_type* One;
};

static Vector4Types Vector4;

static void Vector4Init()
{
    Vector4.New = &V4;
    Vector4.Zero = &_InternalVector4Zero;
    Vector4.One = &_InternalVector4One;
}



/* -------------- Rect  --------------*/
union rect
{
    struct
    {
        float X;
        float Y;
        float Width;
        float Height;
    };
    struct
    {
        v2 Position;
        v2 Size;
    };
    struct
    {
        v4 Vector4;
    };
    float E[4];
};

typedef rect internal_Rect_new(float x, float y, float width, float height);
typedef bool internal_Rect_intersects(rect a, rect b);

static inline rect
Rect(float x, float y, float width, float height)
{
    rect result;

    result.X = x;
    result.Y = y;
    result.Width = width;
    result.Height = height;

    return result;
}

static inline bool
_InternalRectIntersects(rect a, rect b)
{
    bool result = (a.X + a.Width > b.X) && (a.X < b.X + b.Width) && (a.Y + a.Height > b.Y) && (a.Y < b.Y + b.Height);

    return result;
}

struct RectTypes
{
    internal_Rect_new* New;
    internal_Rect_intersects* Intersects;
};

static RectTypes LudoRect;

static void RectInit()
{
    LudoRect.New = &Rect;
    LudoRect.Intersects = &_InternalRectIntersects;
}



/* -------------- Color  --------------*/
union color
{
    struct
    {
        float R;
        float G;
        float B;
        float A;
    };
    struct
    {
        v4 RGBA;
    };
    struct
    {
        v3 RGB;
        float Alpha;
    };
    float E[4];
};

typedef color internal_Color_type(void);
typedef color internal_Color_newRGBA(float, float, float, float);

static inline color
_InternalColor(float r, float g, float b, float a)
{
    color result;

    result.R = r;
    result.G = g;
    result.B = b;
    result.A = a;

    return result;
}

static inline color _InternalColor_White(void) { return {1, 1, 1, 1}; }
static inline color _InternalColor_Black(void) { return {0, 0, 0, 1}; }
static inline color _InternalColor_Transparent(void) { return {0, 0, 0, 0}; }
static inline color _InternalColor_Red(void) { return {1, 0, 0, 1}; }
static inline color _InternalColor_Green(void) { return {0, 1, 0, 1}; }

struct ColorTypes
{
    internal_Color_newRGBA* New;
    internal_Color_type* White;
    internal_Color_type* Black;
    internal_Color_type* Transparent;
};

static ColorTypes Color;

static void ColorInit()
{
    Color.New = &_InternalColor;
    Color.White = &_InternalColor_White;
    Color.Black = &_InternalColor_Black;
    Color.Transparent = &_InternalColor_Transparent;
}



/* -------------- Matrices --------------*/
typedef matrix internal_Matrix_type(void);
typedef matrix internal_Matrix_type1(float);
typedef matrix internal_Matrix_type2(float, float);
typedef matrix internal_Matrix_type3(v3);
typedef matrix internal_Matrix_Projection(float fov, float aspectRatio, float nearClipp, float farClipp);
typedef matrix internal_Matrix_Orthographic(float width, float height, float nearClipp, float farClipp);
typedef matrix internal_Matrix_OffCenter_Orthographic(float left, float right, float bottom, float top, float nearClipp, float farClipp);
typedef matrix internal_Matrix_LookAt(v3 target, v3 position, v3 up);
typedef v2 internal_Matrix_TransformV2(v2 target, matrix m);

struct MatrixTypes
{
    internal_Matrix_type* Identity;
    internal_Matrix_type1* Scale;
    internal_Matrix_type1* RotateX;
    internal_Matrix_type1* RotateY;
    internal_Matrix_type1* RotateZ;
    internal_Matrix_Projection* PerspectiveProjection;
    internal_Matrix_Orthographic* OrthographicProjection;
    internal_Matrix_OffCenter_Orthographic* OrthographicProjectionOffCenter;
    internal_Matrix_LookAt* LookAt;
    internal_Matrix_type* Mirror;
    internal_Matrix_type* Flip;
    internal_Matrix_type3* Translate;
    internal_Matrix_TransformV2* TransformVector2;
    internal_Matrix_type2* Scale2D;
};







#endif
