//  Gurksallad Standard Library
//  Game utility 1.0
//  Create gameloop, renderer, window and other god things to have
//  By David isaksson 2022-11-27
//
//  Depends on SDL2
//
//  Usage
//  Begin with define GSL_SDLGAME_IMPLEMENTATION early on in your code
//  Fill in a struct of type gsl_game_desc with your prefered settings
//      If no renderfunctiona and update function is supplied game will set you up with a demo loop
//  Feed your gamedescriptor to gsl_Game_Create and it will give you a handle back if it succedes.
//  Provide this gamehandle to gsl_Game_Run() and you shuold be good to go.
//  
//  Notes
//  
// TODOS
//      Handle resolution on windowscaling
//      Better fileio on wasm layer
//      Audio services



//#define WINDOWSBUILD
//#define __EMSCRIPTEN__

//  Header
#ifndef GSL_SDLGAME_H
#define GSL_SDLGAME_H

#include "gsl_types.h"

// Forward declarations
struct gsl_game_desc;
struct gsl_game_handle;
struct gsl_game_desc;
struct gsl_game_filehandle;
struct gsl_game_DefaultShader;
struct gsl_game_fileio;
struct gsl_game_services;

typedef gsl_game_filehandle gsl_game_fileio_LoadFile(char*);
typedef bool gsl_game_fileio_SaveFile(char*, uint32, void*);
typedef void gsl_game_fileio_FreeFile(gsl_game_filehandle);
void gsl_internal_TickLoop(gsl_game_handle* gameHandle, gsl_game_services* services);
uint32 gsl_Game_ComposeShader(char* vertexShaderSource, char* fragmentShaderSource);

#define GAME_UPDATE_FUNCTION(name) void name(gsl_game_handle* handle, gsl_game_services* services)
typedef GAME_UPDATE_FUNCTION(gameUpdate);

#define GAME_RENDER_FUNCTION(name) void name(gsl_game_handle* handle, gsl_game_services* services)
typedef GAME_RENDER_FUNCTION(gameRender);
GAME_RENDER_FUNCTION(gsl_GameRenderStubFn);

#define GAME_INITIALIZE_FUNCTION(name) void name(gsl_game_handle* handle, gsl_game_services* services)
typedef GAME_INITIALIZE_FUNCTION(gameInitialize);
GAME_INITIALIZE_FUNCTION(gsl_GameInitializeStubFn);

#ifdef WINDOWSBUILD
    #include "../gl/glew.h"
    #include <gl/GL.h>
    //#include <SDL_opengles2.h>
    #include "../imgui/backends/imgui_impl_sdl.h"
#endif
#define SDL_MAIN_HANDLED
#ifdef __EMSCRIPTEN__
#define GL_GLEXT_PROTOTYPES 1
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_opengl.h>
    #include <SDL2/SDL_opengles2.h>
    #include <emscripten.h>
#endif

#include "../sdl/SDL_video.h"
#include "gsl_sdlsound.h"
#include "gsl_math.h"



// Basic shader for demo only
struct gsl_game_DefaultShader
{
    char*                       DemoVertexShader;   //
    char*                       DemoFragmentShader; //
    uint32                      ProgramId;          // Shader id to use
    uint32                      DemoVBO;            //
    uint32                      DemoVAO;            //
};

struct gsl_game_Timer
{
    float                       DeltaTime;          //
    int64                       QPTime;             //
};

//
struct gsl_game_handle
{
    SDL_Window*                 sdlWindow;          //
    SDL_Renderer*               sdlRenderer;        //
    bool                        Running;            //
    gsl_game_desc*              GameDesc;           //
    SDL_GLContext               GLContext;          //
    gsl_game_DefaultShader      DefaultShader;      //
    gsl_game_sounddevice_desc   SoundDevice;        //
    gsl_game_Timer              Time;               //
};

//
struct gsl_game_filehandle
{
    void                        *Data;              //
    uint32                      ContentSize;        //
};

//
struct gsl_game_desc
{
    char*                       WindowTitle;        //
    int                         WindowWidth;        //
    int                         WindowHeight;       //
    gameInitialize*             InitializeFn;       //
    gameUpdate*                 UpdateFn;           //
    gameRender*                 RenderFn;           //
    bool                        UseImGui;           //  set to true if you want to use imgui
    gsl_game_sounddevice_desc   AudioDevice;        //
};

//
struct gsl_game_fileio
{
    gsl_game_fileio_LoadFile*   LoadFile;           //
    gsl_game_fileio_SaveFile*   SaveFile;           //
    gsl_game_fileio_FreeFile*   FreeFile;           //
};

//
struct gsl_game_services
{
    gsl_game_fileio             FileIO;             //
    gsl_game_sound              Sound;              //
};   



#ifdef WINDOWSBUILD
    #include "gsl_windowslayer.h"
#endif
#ifdef __EMSCRIPTEN__
    #include "gsl_wasmlayer.h"
#endif



GAME_INITIALIZE_FUNCTION(gsl_GameInitializeStubFn)
{
    // Load Shader
    handle->DefaultShader.ProgramId = gsl_Game_ComposeShader(gsl_DefaultVertexShader, gsl_DefaultFragmentShader);

    if(handle->DefaultShader.ProgramId != 0)
    {
        // we have a shader
        GLuint positionId = glGetAttribLocation(handle->DefaultShader.ProgramId, "VertexPos2D");
        if(positionId == -1)
        {
            printf("Could not retreive attributlocation VertexPos2D from Shader\n");
        }
        else
        {
            float vertices[] =
            {
                -0.5f, -0.5f,0.0f,
                0.5f, -0.5f,0.0f,
                0.0f, 0.8f,0.0f
            };

            unsigned int VBO, VAO;
            glGenVertexArrays(1, &VAO);
            glGenBuffers(1, &VBO);

            glBindVertexArray(VAO);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
            glEnableVertexAttribArray(positionId);

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            
            handle->DefaultShader.DemoVAO = VAO;
            handle->DefaultShader.DemoVBO = VBO;
        }
    }

    // Create a testsound
    float length = 0.5f;                         // s
    int samplesize = (int)(length * 2.0f * 48000.0f);    // 2 channels, 48000 smaples per second
    gsl_game_audio_desc testAudio;
    testAudio.BalanceLeft = 1;
    testAudio.BalanceRight = 1;
    testAudio.Data = (int16*)malloc(samplesize * sizeof(int16));
    testAudio.DataLenght = samplesize;
    testAudio.Note = 220.0f;
    testAudio.Volume = 1.0f;

    for(int i = 0; i < samplesize; i+= 2)
    {
        float fadein = (float)i / 5000.0f;
        fadein = Clamp(fadein, 0.0f, 1.0f);
        float fadeout = 1.0f;
        float end = (float)(samplesize -5000);
        if(i > samplesize -5000)
        {
            fadeout = 1.0f - (((float)i - end) / 5000);
        }

        float  sample = 0;
		float tSec = (float)i / 48000;
		float volume = 12000.0f;
		float hz = 220.0f; // Middle A

        float sine = volume * sinf((Pi32 * 2.0f) * hz * tSec);
		//sine += volume * sinf((Pi32 * 2.0f) * hz * 5 * tSec);
		
		sample = sine * fadein * fadeout;
		testAudio.Data[i] = (int16)sample; //Right chanel
		testAudio.Data[i + 1] = (int16)sample; //Left chanel
    }

    gsl_gameaudio_pushaudio(&handle->SoundDevice, &testAudio);
    gsl_gameaudio_pushaudio(&handle->SoundDevice, &testAudio, 300.0f);
}


GAME_RENDER_FUNCTION(gsl_GameRenderStubFn)
{
    if(handle->GameDesc->UseImGui == true)
    {
        bool show_demo_window = true;
        ImGui::ShowDemoWindow(&show_demo_window);

        if(ImGui::Button("Play sound"))
        {
            gsl_game_filehandle fh = services->FileIO.LoadFile("storm.wav");
            gsl_gameaudio_WaveFileData audio = gsl_gameaudio_LoadWaveAudioFile(fh.Data);
            services->FileIO.FreeFile(fh);

            gsl_game_audio_desc wavSoundDescriptor;
            wavSoundDescriptor.Data = audio.Data;
            wavSoundDescriptor.DataLenght = audio.Size;
            wavSoundDescriptor.Note = 220.0f;
            wavSoundDescriptor.Volume = 1.0f;
            wavSoundDescriptor.State = gsl_game_audiostates::New;
            gsl_gameaudio_pushaudio(&handle->SoundDevice, &wavSoundDescriptor);
        }
    }
    glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw here
    glUseProgram(handle->DefaultShader.ProgramId);
    glBindVertexArray(handle->DefaultShader.DemoVAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);

}

#endif


//  Implementation
#ifdef GSL_SDLGAME_IMPLEMENTATION


static void gsl_Game_PrintShaderLog(GLuint shader)
{
	if( glIsShader(shader))
	{
		int infoLogLength = 0;
		int maxLength = infoLogLength;
		
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		char* infoLog = (char*)calloc(1, maxLength);
		glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if( infoLogLength > 0 )
		{
			printf("%s\n", infoLog);
		}
		free(infoLog);
	}
	else
	{
		printf( "Name %d is not a shader\n", shader );
	}
}

uint32 gsl_Game_ComposeShader(char* vertexShaderSource, char* fragmentShaderSource)
{
    GLchar *vertexSourceArray[] = { vertexShaderSource };
    GLchar *fragmentSourceArray[] = { fragmentShaderSource };

    GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    // Compile vertexshader
    glShaderSource(vertexShaderId, 1, vertexSourceArray, 0);
    glCompileShader(vertexShaderId);
    GLint vertexCompilationResult = GL_FALSE; 
    glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &vertexCompilationResult);
    if(vertexCompilationResult == GL_FALSE)
    {
        // Todo print shader log error message here.. Will be hard to debug shaders without
        printf("Error compiling vertexshader...\n");
        gsl_Game_PrintShaderLog(vertexShaderId);
        return 0;
    }

    // Compile Fragmentshader
    glShaderSource(fragmentShaderId, 1, fragmentSourceArray, 0);
    glCompileShader(fragmentShaderId);
    GLint fragmentCompilationResult = GL_FALSE; 
    glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &fragmentCompilationResult);
    if(fragmentCompilationResult == GL_FALSE)
    {
        // Todo print shader log error message here.. Will be hard to debug shaders without
        printf("Error compiling fragmentShader...\n");
        gsl_Game_PrintShaderLog(fragmentShaderId);
        return 0;
    }
    
    GLuint programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);

    // Link
    glLinkProgram(programId);
    GLint programLinkResult = GL_FALSE; 
    glGetProgramiv(programId, GL_LINK_STATUS, &programLinkResult);
    if(programLinkResult == GL_FALSE)
    {
        // Todo print shader log error message here.. Will be hard to debug shaders without
        printf("Error linking shaders...\n");
        gsl_Game_PrintShaderLog(programId);
        return 0;
    }

    return programId;
}


static void InitImGui()
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
#ifdef WINDOWSBUILD
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
#endif
    //io.ConfigViewportsNoAutoMerge = true;
    //io.ConfigViewportsNoTaskBarIcon = true;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

}



void gsl_internal_TickLoop(gsl_game_handle* gameHandle, gsl_game_services* services)
{
    Uint64 qpTimeStart = SDL_GetPerformanceCounter();
    gameHandle->Time.DeltaTime = (qpTimeStart - gameHandle->Time.QPTime) / (float)SDL_GetPerformanceFrequency() * 1000.0f;
    gameHandle->Time.QPTime = qpTimeStart;

    // FrameTick and pause
    if(gameHandle->GameDesc->UpdateFn != 0) 
        gameHandle->GameDesc->UpdateFn(gameHandle, services);

    if(gameHandle->GameDesc->UseImGui == true)
    {
        ImGui_ImplSDL2_NewFrame();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui::NewFrame();
        ImGui::DockSpaceOverViewport(0, ImGuiDockNodeFlags_PassthruCentralNode);
    }   

    if(gameHandle->GameDesc->RenderFn != 0) 
        gameHandle->GameDesc->RenderFn(gameHandle, services);

    if(gameHandle->GameDesc->UseImGui == true)
    {
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        ImGuiIO& io = ImGui::GetIO();
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            SDL_Window* backup_current_window = SDL_GL_GetCurrentWindow();
            SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
        }
    }
    SDL_GL_SwapWindow(gameHandle->sdlWindow);
}




static gsl_game_handle* gsl_Game_Create(gsl_game_desc* gameDescriptor)
{
    gsl_game_handle* result = (gsl_game_handle*)calloc(1, sizeof(gsl_game_handle));

    result->Running = false;
    result->GameDesc = gameDescriptor;

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO) < 0)
    {
        printf("Error initiliazing SDL\n");
        return 0;
    }

    // IS it demo
    if(result->GameDesc->InitializeFn == 0)
    {
        // If no init function is provided, provide a demo function instead...
        printf("Demo initializer...\n");
        result->GameDesc->InitializeFn = &gsl_GameInitializeStubFn;
    }

    if(result->GameDesc->RenderFn == 0)
    {
        printf("Demo renderloop set...\n");
        result->GameDesc->RenderFn = &gsl_GameRenderStubFn;
    }

// Setup a sdl build
#ifdef __EMSCRIPTEN__

    SDL_WindowFlags windowFlags = (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);
    SDL_CreateWindowAndRenderer(gameDescriptor->WindowWidth, gameDescriptor->WindowHeight, windowFlags, &result->sdlWindow, 0);

    if(result->sdlWindow == 0)
    {
        //TODO Better logging
        printf("Could not create sdl window\n");
        free(result);
        return 0;
    }
    
    if(gsl_internalwasm_game_initializeOpenGL(result) == false)
    {
        // TODO Better logging...
        printf("Error creatig Opengl context...\n");
        return 0;
    }
    printf("B\n"); 
    //Init ImGui if enabled
    if(gameDescriptor->UseImGui)
    {
        InitImGui();

        const char* glsl_version = "#version 100";
        ImGui_ImplSDL2_InitForOpenGL(result->sdlWindow, result->GLContext);
        ImGui_ImplOpenGL3_Init(glsl_version);
    }

#endif


// Setup Windows build
#ifdef WINDOWSBUILD

    SDL_WindowFlags windowFlags = (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);
    result->sdlWindow = SDL_CreateWindow(
        gameDescriptor->WindowTitle,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        gameDescriptor->WindowWidth,
        gameDescriptor->WindowHeight,
        windowFlags
    );

    if(result->sdlWindow == 0)
    {
        //TODO Better logging
        printf("Could not create sdl window\n");
        free(result);
        return 0;
    }

    if(gsl_internalwin_game_initializeOpenGL(result) == false)
    {
        // TODO Better logging...
        printf("Error creatig Opengl context...\n");
        return 0;
    }

    //Init ImGui if enabled
    if(gameDescriptor->UseImGui)
    {
        InitImGui();

        const char* glsl_version = "#version 130";
        ImGui_ImplSDL2_InitForOpenGL(result->sdlWindow, result->GLContext);
        ImGui_ImplOpenGL3_Init(glsl_version);
    }
#endif
    
    gsl_GameSound_Initialize(&result->SoundDevice);

    printf("Game init success\n");
    return result;
}



static void gsl_Game_Run(gsl_game_handle* gameHandle)
{
    gsl_game_services services = {};
    GetPlatformServiceFunctions(&services);
    gameHandle->Time.QPTime = SDL_GetPerformanceCounter();

    gameHandle->Running = true;

    if(gameHandle->GameDesc->InitializeFn != 0) 
    {
        gameHandle->GameDesc->InitializeFn(gameHandle, &services);
    }
    else
    {
        printf("No Initialize function is specified\n");
    }

#ifdef __EMSCRIPTEN__
    _gsl_internal_gameHandle = gameHandle;
    _gsl_internal_gameServices = &services;
    printf("WASM running... %s\n", _gsl_internal_gameHandle->GameDesc->WindowTitle);
    emscripten_set_main_loop(&gsl_internal_wasmMainLoop, 0, 1);
#endif

#ifdef WINDOWSBUILD
    gsl_internal_windowsMainLoop(gameHandle, &services);
#endif

}

#endif

//  Changelog
//  2022-11-27      - File Created
//                  - Basic pipeline for Wasm and Windows
//  2022-12-10      - ImGui Implemetation added for windows and wasm, add UseImgui in GameDescription to enable
//  2022-12-18      - Sound is implemented througe gsl_gamesound.h