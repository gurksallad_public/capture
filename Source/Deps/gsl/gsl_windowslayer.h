#ifndef GSL_SDL_WINDOWSLAYER_H
#define GSL_SDL_WINDOWSLAYER_H

#include "gsl_types.h"
#include "../imgui/imgui.h"
#include "../imgui/imgui.cpp"
#include "../imgui/imgui_widgets.cpp"
#include "../imgui/imgui_draw.cpp"
#include "../imgui/imgui_tables.cpp"
#include "../imgui/imgui_demo.cpp"

#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#include "../imgui/backends/imgui_impl_opengl3.h"
#include "../imgui/backends/imgui_impl_opengl3.cpp"
#include "../sdl/SDL.h"
#include "../sdl/SDL_opengl.h"
#include "../imgui/backends/imgui_impl_sdl.cpp"

#include <windows.h>

#endif

//  Implementation
#ifdef GSL_SDLGAME_IMPLEMENTATION

    // static test shader to get going
    static char* gsl_DefaultVertexShader = 
    "#version 140\n"
    "\n" 
    "   in vec2 VertexPos2D;\n"
    "\n"
    "   void main()\n"
    "   {\n"
    "       gl_Position = vec4(VertexPos2D.x, VertexPos2D.y, 0, 1);\n"
    "   }\n"
    "\n"
    "\n";

    static char* gsl_DefaultFragmentShader = 
    "#version 140\n"
    "\n" 
    "   out vec4 Fragment;\n"
    "\n"
    "   void main()\n"
    "   {\n"
    "       Fragment = vec4(0.8, 0.1, 0.2, 1.0);\n" // Dark bluish outputcolor....
    "   }\n"
    "\n"
    "\n";



    //
    inline uint32 gsl_game_SafeTruncateUInt64(uint64 value)
    {
        Assert(value<= 0xFFFFFFFF);
        uint32 result = (uint32)value;
        return (uint32)value;
    }



    //
    static void gsl_internal_windowsFreeFile(gsl_game_filehandle fileHandle)
    {
        if(fileHandle.Data)
        {
            VirtualFree(fileHandle.Data, 0, MEM_RELEASE);
        }
        fileHandle.ContentSize = 0;
        fileHandle.Data = 0;
    }



    //
    static bool gsl_internal_windowsSaveFile(char* filePathName, uint32 contentSize, void* data)
    {
        bool result = false;

        HANDLE fileHandle = CreateFileA(filePathName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
        if(fileHandle != INVALID_HANDLE_VALUE)
        {
            DWORD byteCount = 0;
            if(WriteFile(fileHandle, data, contentSize, &byteCount, 0))
            {
                result = (byteCount == contentSize);
            }
            else
            {
                // TODO: Better error logging
                printf("Error writing file to disc");
            }
            CloseHandle(fileHandle);
        }
        else
        {
            // TODO: Better error logging
            printf("Error writing file to disc");
        }

        return result;
    }



    //
    gsl_game_filehandle gsl_internal_windowsLoadFile(char* filePathName)
    {
        gsl_game_filehandle result = {};

        HANDLE fileHandle = CreateFileA(filePathName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);
        if(fileHandle != INVALID_HANDLE_VALUE)
        {
            LARGE_INTEGER fileSize64 = {};
            if(GetFileSizeEx(fileHandle, &fileSize64))
            {
                uint32 fileSize32 = gsl_game_SafeTruncateUInt64((uint64)fileSize64.QuadPart);
                result.Data = VirtualAlloc(0, fileSize32, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

                if(result.Data)
                {
                    DWORD byteCount = 0;
                    if(ReadFile(fileHandle, result.Data, fileSize32, &byteCount, 0))
                    {
                        if(byteCount == fileSize32)
                        {
                            result.ContentSize = fileSize32;
                        }
                        else
                        {
                            gsl_internal_windowsFreeFile(result);
                            // TODO: Better error logging
                            printf("Error reading file from disc");
                        }
                    }
                    else
                    {
                        gsl_internal_windowsFreeFile(result);
                        // TODO: Better error logging
                        printf("Error reading file from disc");
                    }
                }
                else
                {
                    // TODO: Better error logging
                    printf("Error reading file from disc");
                }
            }
            else
            {
                // TODO: Better error logging
                printf("Error reading file from disc");
            }
            CloseHandle(fileHandle);
        }
        else
        {
            // TODO: Better error logging
            printf("Error reading file from disc");
        }

        return result;
    }



    //
    static void gsl_internal_windowsMainLoop(gsl_game_handle* gameHandle, gsl_game_services* services)
    {
        while (gameHandle->Running) 
        {
            SDL_Event event;
            while (SDL_PollEvent(&event))
            {
                if(gameHandle->GameDesc->UseImGui) ImGui_ImplSDL2_ProcessEvent(&event);
                if (event.type == SDL_QUIT)
                    gameHandle->Running = false;
                if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(gameHandle->sdlWindow))
                    gameHandle->Running = false;
            }
            
            gsl_internal_TickLoop(gameHandle, services);
        }
    }



    // returns false if Gl context and renderer fails to initialize
    static bool gsl_internalwin_game_initializeOpenGL(gsl_game_handle* handle)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        handle->GLContext = SDL_GL_CreateContext(handle->sdlWindow);
        if(handle->GLContext == 0)
        {
            // TODO logging
            printf("GL context creation faild\n");
            return false;
        }

        GLenum glError = glewInit();
        if(glError != GLEW_OK)
        {
            // TODO Logging
            printf("Glew init faild...\n");
            return false;
        }

        handle->DefaultShader.DemoVertexShader = gsl_DefaultVertexShader;
        if(handle->GameDesc->InitializeFn == 0)
        {
            // If no init function is provided, provide a demo function instead...
            printf("Demo initializer...\n");
            handle->GameDesc->InitializeFn = &gsl_GameInitializeStubFn;
        }

        if(handle->GameDesc->RenderFn == 0)
        {
            printf("Demo renderloop set...\n");
            handle->GameDesc->RenderFn = &gsl_GameRenderStubFn;
        }
        return true;
    }
    


    static void GetPlatformServiceFunctions(gsl_game_services* services)
    {
        services->FileIO.LoadFile = &gsl_internal_windowsLoadFile;
        services->FileIO.SaveFile = &gsl_internal_windowsSaveFile;
        services->FileIO.FreeFile = &gsl_internal_windowsFreeFile;
    }

#endif