
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb/stb_image_write.h"

void SaveCapturedBitmap(void* bitmap, int width, int height, char* filename)
{
    int comp = 4;       // 'comp' == 1=Y, 2=YA, 3=RGB, 4=RGBA.
    int quality = 92;   // 0 - 100 where 100 is best quality and 0 is highest  compresion
    
    stbi_flip_vertically_on_write(1);
    int result = stbi_write_jpg(filename, width, height, comp, (void*)bitmap, quality);
    //int result = stbi_write_bmp(filename, width, height, comp);
}



int CaptureScreen(HWND hWnd, rect sourceRect, rect destinationRect, char* fileName)
{
    HDC hdcScreen;
    HDC hdcWindow;
    HDC hdcMemDC = NULL;
    HBITMAP hbmScreen = NULL;
    BITMAP bmpScreen;
    DWORD dwBytesWritten = 0;
    HANDLE hFile = NULL;
    char* lpbitmap = NULL;
    HANDLE hDIB = NULL;
    DWORD dwBmpSize = 0;

    hdcScreen = GetDC(NULL);
    hdcWindow = GetDC(NULL);
    hdcMemDC = CreateCompatibleDC(0);
    if (!hdcMemDC)  goto done;

    SetStretchBltMode(hdcScreen, HALFTONE);
    hbmScreen = CreateCompatibleBitmap(hdcWindow, (int)destinationRect.Width, (int)destinationRect.Height);
    if (!hbmScreen) goto done;

    SelectObject(hdcMemDC, hbmScreen);
    if (!StretchBlt(hdcMemDC,
        0, 0,
        (int)destinationRect.Width, (int)destinationRect.Height,
        hdcScreen,
        (int)sourceRect.X, (int)sourceRect.Y,
        (int)(sourceRect.Width - sourceRect.X),
        (int)(sourceRect.Height - sourceRect.Y),
        SRCCOPY))
    {
        goto done;
    }

    GetObject(hbmScreen, sizeof(BITMAP), &bmpScreen);

    BITMAPINFOHEADER bi;
    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = bmpScreen.bmWidth;
    bi.biHeight = bmpScreen.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = 32;
    bi.biCompression = BI_RGB;
    bi.biSizeImage = 0;
    bi.biXPelsPerMeter = 0;
    bi.biYPelsPerMeter = 0;
    bi.biClrUsed = 0;
    bi.biClrImportant = 0;

    dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;
    hDIB = GlobalAlloc(GHND, dwBmpSize);
    lpbitmap = (char*)GlobalLock(hDIB);

    GetDIBits(hdcWindow, hbmScreen, 0,
        (UINT)bmpScreen.bmHeight,
        lpbitmap,
        (BITMAPINFO*)&bi, DIB_RGB_COLORS);

    // bitswap to correct rbg format
    uint8* row = (uint8*)lpbitmap;
    for (int y = 0; y < destinationRect.Height; ++y)
	{
        uint32* wpixel = (uint32*)row;// +(int)destinationRect.Width);

		for (int x = 0; x < destinationRect.Width; ++x)
		{
            uint8* bits = (uint8*)wpixel;
            uint8 b = *bits;
            uint8 g = *(bits + 1);
            uint8 r = *(bits + 2);
            uint8 a = *(bits + 3);
            *wpixel = (((uint8)a << 24) | ((uint8)b << 16) | ((uint8)g << 8) | (uint8)r);
			wpixel++;
		}

		row += (int)destinationRect.Width * 4;
	}

    SaveCapturedBitmap(lpbitmap, (int)destinationRect.Width, (int)destinationRect.Height, fileName);

    GlobalUnlock(hDIB);
    GlobalFree(hDIB);

    // Clean up.
done:
    DeleteObject(hbmScreen);
    DeleteObject(hdcMemDC);
    ReleaseDC(NULL, hdcScreen);
    ReleaseDC(hWnd, hdcWindow);

    return 0;
}