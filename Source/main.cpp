#include <stdio.h>
#define GSL_SDLGAME_IMPLEMENTATION
#include "Deps/gsl/gsl_sdlgame.h"

#include "core.cpp"



void Render()
{
    
}

int main(int argc, char** argv)
{
    gsl_game_desc game = {};
    game.WindowTitle = (char*)"SceenCapture";
    game.WindowWidth = 1280;
    game.WindowHeight = 800;
    game.UseImGui = true;
    game.InitializeFn = Init_Game;
    game.UpdateFn = Update_Game;
    game.RenderFn = Render_Game;

    gsl_game_handle* gameHandle = gsl_Game_Create(&game);

    if(gameHandle)
    {
        gsl_Game_Run(gameHandle);
    }
    else
    {
        printf("Error createing game... ");
        // TODO: Error logging
    }

}