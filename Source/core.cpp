#include "core.h"
#include "deps/imgui/imgui.h"
//#include "deps/gsl/gsl_sdlgame.h"
#include "screenCapture.cpp"
#include "ffmpg-utils.cpp"
#include "ImGuiThemes.cpp"




static AppWindows appWindows;
static ProjectSettings currentProject;
static ImGuiFonts imguiFonts;


#include "systems.cpp"
#include "project.cpp"



GAME_INITIALIZE_FUNCTION(Init_Game)
{
    ImGuiTheme_GslDarkBlue();
    ImGuiIO& io = ImGui::GetIO();
    ImFontConfig config;
    config.MergeMode = true;
    static const ImWchar icon_ranges[] = { 0xe005, 0xf8ff, 0 };
    imguiFonts.DefaultFont = io.Fonts->AddFontFromFileTTF("liberation-mono.ttf", 15, 0);
    imguiFonts.LargeFont = io.Fonts->AddFontFromFileTTF("liberation-mono.ttf", 18, 0);
    imguiFonts.FontAwesome = io.Fonts->AddFontFromFileTTF("Font Awesome 6 Free-Solid-900.otf", 20, &config, icon_ranges);

    io.Fonts->Build();

    appWindows.DemoWindow = false;
    appWindows.CaptureWindow = true;
    appWindows.SystemInformation = true;
    appWindows.ProjectWindow = false;

    int resX = GetSystemMetrics(SM_CXSCREEN);
    int resY = GetSystemMetrics(SM_CYSCREEN);
    currentProject.CaptureRect[0] = 0;
    currentProject.CaptureRect[1] = 0;
    currentProject.CaptureRect[2] = resX;
    currentProject.CaptureRect[3] = resY;
    currentProject.ShowPreviewRect = false;
    currentProject.CaptureInterval = 5000;
    currentProject.DestinationSize[0] = 1920;
    currentProject.DestinationSize[1] = 1080;
    currentProject.CaptureState = 0;
    snprintf(currentProject.CapturePath, 512, "G:\\ScreenCaptures");
    snprintf(currentProject.ProjectName, 512, "ProjectName");

}

GAME_UPDATE_FUNCTION(Update_Game)
{

    if(currentProject.CaptureState == 1)
    {
        // Capturing
        // need delta time
        currentProject.RecordTime += handle->Time.DeltaTime;

        if(currentProject.RecordTime > 5000)
        {
            currentProject.RecordTime = 0;
            currentProject.CaptureCount++;
        }
    }

}

GAME_RENDER_FUNCTION(Render_Game)
{
    if(appWindows.DemoWindow == true) ImGui::ShowDemoWindow(&appWindows.DemoWindow);
    ShowSystemInformationWindow();
    ShowProjectWindow();
    if(currentProject.ShowPreviewRect) RenderPreviewRect();


    if(ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("System"))
        {
            if (ImGui::MenuItem("New Project")) 
            {
                appWindows.ProjectWindow = true;
            }
            if (ImGui::MenuItem("Save")) 
            {
                
            }
            if (ImGui::MenuItem("Quit")) 
            {
                // TODO: Ask before quiting...
                handle->Running = false;
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Capture"))
        {
            if (ImGui::MenuItem("Start capture")) {}
            if (ImGui::MenuItem("Pause capture")) {}
            if (ImGui::MenuItem("Stop capture")) {}
            if (ImGui::MenuItem("Settings")) {}
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Windows"))
        {
            if (ImGui::MenuItem("Show System information")) { appWindows.SystemInformation = true; }

            if (ImGui::MenuItem("Show ImGui DemoWindow")) { appWindows.DemoWindow = true; }
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }



    glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

}