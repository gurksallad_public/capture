#ifndef CORE_H
#define CORE_H

struct AppWindows
{
    bool CaptureWindow;
    bool DemoWindow;
    bool SystemInformation;
    bool ProjectWindow;
};

struct ProjectSettings
{
    char ProjectName[512];
    char CapturePath[512];
    int CaptureRect[4];
    int DestinationSize[2];
    bool ShowPreviewRect;
    float CaptureInterval;
    int CaptureState;
    float RecordTime;
    int CaptureCount;
};

struct ImGuiFonts
{
    ImFont* DefaultFont;
    ImFont* LargeFont;
    ImFont* FontAwesome;
};

#endif