#include "Deps/imgui/imgui.h"
#include <windows.h>



void ShowSystemInformationWindow()
{   
    if(appWindows.SystemInformation == false) return;
    
    char buffer[255];

    ImGui::Begin("System information", &appWindows.SystemInformation, ImGuiWindowFlags_AlwaysAutoResize);

    // Monitor counts
    int monitorCount = GetSystemMetrics(SM_CMONITORS);
    snprintf(buffer, 255, "%i", monitorCount);
    ImGui::InputText("Displays", buffer, 255, ImGuiInputTextFlags_ReadOnly);

    // Screen resoulution
    int resX = GetSystemMetrics(SM_CXSCREEN);
    int resY = GetSystemMetrics(SM_CYSCREEN);
    snprintf(buffer, 255, "%i * %i", resX, resY);
    ImGui::InputText("Resolution", buffer, 255, ImGuiInputTextFlags_ReadOnly);


    // Virtual Screen resoulution
    int vResX = GetSystemMetrics(SM_CXVIRTUALSCREEN);
    int vResY = GetSystemMetrics(SM_CYVIRTUALSCREEN);
    snprintf(buffer, 255, "%i * %i", vResX, vResY);
    ImGui::InputText("Virtual Res.", buffer, 255, ImGuiInputTextFlags_ReadOnly);


    // Monitor colorformats
    int monitorColorFormat = GetSystemMetrics(SM_SAMEDISPLAYFORMAT);
    snprintf(buffer, 255, "%i", monitorColorFormat);
    ImGui::InputText("Same displayfromat", buffer, 255, ImGuiInputTextFlags_ReadOnly);

    ImGui::End();
}