#include "Deps/gsl/gsl_types.h"
#include <stdio.h>
//#include <io.h>



// create videos of a sequenze of frames
void ffmpg_push()
{
	// static uint8 frame[720][1280][3];
	// int fds[2], x,y,t, status;
	// FILE *ffmpeg;

	// /* Launch ffmpeg */

	// char *argv[] = {
	// 	"ffmpeg",
	// 	"-loglevel", "warning",
	// 	"-stats",
	// 	"-f", "rawvideo",
	// 	"-pixel_format", "rgb24",
	// 	"-video_size", "1280x720",
	// 	"-framerate", "30",
	// 	"-i", "-",
	// 	"-pix_fmt", "yuv420p",
	// 	"output.mp4",
	// 	NULL
	// };

	// if (pipe(fds) == -1)
	// 	err(1, "pipe");
}



/*
  configuration: 
  --prefix=/ffbuild/prefix 
  --pkg-config-flags=--static 
  --pkg-config=pkg-config 
  --cross-prefix=x86_64-w64-mingw32- 
  --arch=x86_64 
  --target-os=mingw32 
  --enable-gpl 
  --enable-version3 
  --disable-debug 
  --disable-w32threads 
  --enable-pthreads 
  --enable-iconv 
  --enable-libxml2 
  --enable-zlib 
  --enable-libfreetype 
  --enable-libfribidi 
  --enable-gmp 
  --enable-lzma 
  --enable-fontconfig 
  --enable-libvorbis 
  --enable-opencl 
  --disable-libpulse 
  --enable-libvmaf 
  --disable-libxcb 
  --disable-xlib 
  --enable-amf 
  --enable-libaom 
  --enable-libaribb24 
  --enable-avisynth 
  --enable-chromaprint 
  --enable-libdav1d 
  --enable-libdavs2 
  --disable-libfdk-aac 
  --enable-ffnvcodec 
  --enable-cuda-llvm 
  --enable-frei0r 
  --enable-libgme 
  --enable-libkvazaar 
  --enable-libass 
  --enable-libbluray 
  --enable-libjxl 
  --enable-libmp3lame 
  --enable-libopus 
  --enable-librist 
  --enable-libssh 
  --enable-libtheora 
  --enable-libvpx 
  --enable-libwebp 
  --enable-lv2 
  --disable-libmfx 
  --enable-libvpl 
  --enable-openal 
  --enable-libopencore-amrnb 
  --enable-libopencore-amrwb 
  --enable-libopenh264 
  --enable-libopenjpeg 
  --enable-libopenmpt 
  --enable-librav1e 
  --enable-librubberband 
  --enable-schannel 
  --enable-sdl2 
  --enable-libsoxr 
  --enable-libsrt 
  --enable-libsvtav1 
  --enable-libtwolame 
  --enable-libuavs3d 
  --disable-libdrm 
  --disable-vaapi 
  --enable-libvidstab 
  --enable-vulkan 
  --enable-libshaderc 
  --enable-libplacebo 
  --enable-libx264 
  --enable-libx265 
  --enable-libxavs2 
  --enable-libxvid 
  --enable-libzimg 
  --enable-libzvbi 
  --extra-cflags=-DLIBTWOLAME_STATIC 
  --extra-cxxflags= 
  --extra-ldflags=-pthread 
  --extra-ldexeflags= 
  --extra-libs=-lgomp 
  --extra-version=20221227

  */