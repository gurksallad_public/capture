#include "Deps/imgui/imgui.h"
#include <windows.h>
#include "deps/gsl/gsl_types.h"
#include "core.h"
#include "Deps/IconsFontAwesome.h"



void RecoringCaptureInformation()
{
    ImGui::NewLine();
    ImGui::Separator();
    ImGui::NewLine();

    ImGui::PushFont(imguiFonts.FontAwesome);
    ImGui::Text("%s", ICON_FA_CIRCLE_PLAY);
    ImGui::PopFont();
    ImGui::SameLine();
    ImGui::PushFont(imguiFonts.LargeFont);
    ImGui::Text("Recoring...");
    ImGui::PopFont();

    ImGui::Text("Count: %i", currentProject.CaptureCount);

    ImGui::Indent();
    ImGui::Unindent();

}

void PausedCaptureInformation()
{
    ImGui::NewLine();
    ImGui::Separator();
}

void ShowProjectWindow()
{   
    if(appWindows.ProjectWindow == false) return;


    ImGui::Begin("New Project", &appWindows.ProjectWindow);

    ImGui::PushFont(imguiFonts.FontAwesome);
    ImGui::Text("%s", ICON_FK_COG);
    ImGui::PopFont();
    ImGui::SameLine();
    ImGui::PushFont(imguiFonts.LargeFont);
    ImGui::Text("Settings");
    ImGui::PopFont();
    ImGui::Spacing();
    ImGui::Indent();

    ImGui::Text("Project name");
    ImGui::PushItemWidth(-1);
    ImGui::InputText("##Project name", currentProject.ProjectName, 512);
    ImGui::PopItemWidth();
    ImGui::Spacing();

    ImGui::Text("Capture path");
    ImGui::PushItemWidth(-1);
    ImGui::InputText("##Capture path", currentProject.CapturePath, 512);
    ImGui::PopItemWidth();
    ImGui::Spacing();

    ImGui::Text("Capture rectangle");
    ImGui::PushItemWidth(-1);
    ImGui::InputInt4("##Capture Rect", currentProject.CaptureRect);
    ImGui::PopItemWidth();

    if(ImGui::Button("Preview"))
    {
        currentProject.ShowPreviewRect = !currentProject.ShowPreviewRect;
        if(currentProject.ShowPreviewRect == false)
        {
            int vResX = GetSystemMetrics(SM_CXVIRTUALSCREEN);
            int vResY = GetSystemMetrics(SM_CYVIRTUALSCREEN);
            RECT rect = { 0, 0, vResX, vResY }; 
            RedrawWindow(0,&rect,0, RDW_ERASE | RDW_INVALIDATE | RDW_ALLCHILDREN);
        }
    }

    ImGui::SameLine();
    if(ImGui::Button("TestCapture"))
    {
        // Capture screenrect to test file output
        rect clipRect = Rect((float)currentProject.CaptureRect[0], (float)currentProject.CaptureRect[1], (float)currentProject.CaptureRect[2], (float)currentProject.CaptureRect[3]);
        rect destRect = Rect(0.0f, 0.0f, (float)currentProject.DestinationSize[0], (float)currentProject.DestinationSize[1]);
        CaptureScreen(GetActiveWindow(), clipRect, destRect, "testFile.jpg");
    }
    ImGui::Spacing();

    ImGui::Text("Outputresolution");
    ImGui::PushItemWidth(-1);
    ImGui::InputInt2("##Outputresolution", currentProject.DestinationSize);
    ImGui::PopItemWidth();

    ImGui::Text("Capture delta (ms)");
    ImGui::PushItemWidth(-1);
    ImGui::DragFloat("##Capture delta", &currentProject.CaptureInterval, 100, 500, 30000);
    ImGui::PopItemWidth();

    ImGui::Unindent();

    ImGui::NewLine();

    ImGui::PushFont(imguiFonts.FontAwesome);
    ImGui::Text("%s", ICON_FA_KEYBOARD);
    ImGui::PopFont();
    ImGui::SameLine();
    ImGui::PushFont(imguiFonts.LargeFont);
    ImGui::Text("Controls");
    ImGui::PopFont();
    ImGui::Indent();

    ImGui::PushFont(imguiFonts.FontAwesome);
    if(currentProject.CaptureState == 1) ImGui::PushStyleColor(ImGuiCol_Text, { 1.0f, 0.0f, 0.0f, 1.0f });
    if(ImGui::Button(ICON_FA_CIRCLE_PLAY " Start", { 100.0f, 36.0f}) && currentProject.CaptureState != 1)
    {
        // begin capture screen
        currentProject.CaptureState = 1;
        ImGui::PushStyleColor(ImGuiCol_Text, { 1.0f, 0.0f, 0.0f, 1.0f });
    }
    if(currentProject.CaptureState == 1) ImGui::PopStyleColor();
    ImGui::SameLine();
    if(currentProject.CaptureState == 2) ImGui::PushStyleColor(ImGuiCol_Text, { 1.0f, 1.0f, 0.0f, 1.0f });
    if(ImGui::Button(ICON_FA_CIRCLE_PAUSE " Pause", { 100.0f, 36.0f}) && currentProject.CaptureState == 1)
    {
        // pause capture screen
        currentProject.CaptureState = 2;
        ImGui::PushStyleColor(ImGuiCol_Text, { 1.0f, 0.0f, 0.0f, 1.0f });
    }
    if(currentProject.CaptureState == 2) ImGui::PopStyleColor();
    ImGui::SameLine();
    if(ImGui::Button(ICON_FA_CIRCLE_STOP " Stop", { 100.0f, 36.0f}))
    {
        // stop capture screen
        currentProject.CaptureState = 0;
    }
    ImGui::PopFont();

    ImGui::Unindent();
    
    if(currentProject.CaptureState == 1)
    {
        RecoringCaptureInformation();
    }

    if(currentProject.CaptureState == 2)
    {
        PausedCaptureInformation();
    }

    ImGui::End();
}

static void RenderPreviewRect()
{
    if(currentProject.ShowPreviewRect == false) return;

    /* Calling GetDC with argument 0 retrieves the desktop's DC */
    HDC hDC_Desktop = GetDC(0);

    /* Draw a simple blue rectangle on the desktop */
    RECT topLine = { currentProject.CaptureRect[0], currentProject.CaptureRect[1], currentProject.CaptureRect[2], currentProject.CaptureRect[1] + 2 };
    RECT bottomLine = { currentProject.CaptureRect[0], currentProject.CaptureRect[3] - 2, currentProject.CaptureRect[2], currentProject.CaptureRect[3] };
    RECT leftLine = { currentProject.CaptureRect[0], currentProject.CaptureRect[1], currentProject.CaptureRect[0] + 2, currentProject.CaptureRect[3] };
    RECT rightLine = { currentProject.CaptureRect[2] - 2, currentProject.CaptureRect[1], currentProject.CaptureRect[2], currentProject.CaptureRect[3] };
    HBRUSH rectColor = CreateSolidBrush(RGB(255, 128, 80));
    FillRect(hDC_Desktop, &topLine, rectColor);
    FillRect(hDC_Desktop, &bottomLine, rectColor);
    FillRect(hDC_Desktop, &leftLine, rectColor);
    FillRect(hDC_Desktop, &rightLine, rectColor);

}



